# ---- agile-data-science
library(tidyverse)

set.seed(143)

t_ <- list(
  line_size = 2,
  colors = function(...) {
    colors <- RColorBrewer::brewer.pal(4, "Set2")
    names(colors) <- c("green", "orange", "blue", "pink")
    unname(colors[unlist(list(...))])
  }
)

Velocity <- tibble(
  week = 1:10,
  velocity = 1:10
) %>%
  mutate(
    velocity = velocity + rnorm(length(velocity))
  )

Estimation <- tibble(
  week = 1:10,
  estimation_accuracy = 1:10
) %>%
  mutate(
    estimation_accuracy = estimation_accuracy + rnorm(length(estimation_accuracy))
  )


base_plot <- ggplot() +
  scale_x_continuous(breaks = 1:10) +
  scale_y_continuous(breaks = 1:10) +
  theme(
    axis.text = element_blank(),
    axis.ticks = element_blank(),
    panel.grid.minor = element_blank()
  ) +
  coord_cartesian(xlim = c(0, 11), ylim = c(0, 11))

velocity_plot <- (base_plot %+% Velocity) +
  aes(week, velocity) +
  geom_line(size = t_$line_size, color = t_$colors("blue")) +
  labs(
    x = "week",
    y = "velocity",
    subtitle = "The velocity of the team improves."
  )

estimation_plot <- (base_plot %+% Estimation) +
  aes(week, estimation_accuracy) +
  geom_line(size = t_$line_size, color = t_$colors("green")) +
  labs(
    x = "week",
    y = "estimation accuracy",
    subtitle = "The ability to estimate improves."
  )
